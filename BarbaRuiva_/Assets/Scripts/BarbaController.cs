﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarbaController : MonoBehaviour
{
    [Header("Parameters")]
    [SerializeField] float speed;
    [SerializeField] float pickDuration;
    [Header("References")]
    [SerializeField] Transform spriteT;
    [SerializeField] GameObject[] directions;
    [SerializeField] GameObject diveCam;

    Rigidbody2D rb;
    Animator animator;
    BoxCollider2D boxCollider2D;

    List<Wearing> wearing = new List<Wearing>();

    Vector3 originalScale;


    bool canControl;

    [HideInInspector]
    public bool diving;

    bool canDiveFromUpwards;
    bool canDiveFromRight;
    bool canDiveFromDownwards;
    bool canDiveFromLeft;

    bool canJump;
    bool canExit;

    bool showDirections;


    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }
    // Start is called before the first frame update
    void Start()
    {
        originalScale = spriteT.transform.localScale;
        canControl = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (canControl)
        {
            Movimentation();
            if (Input.GetKeyDown(KeyCode.O))
                DiveRun();
            if (Input.GetKeyDown(KeyCode.I))
                StartCoroutine(PickRoutine(pickDuration));
        }
                     
        if (showDirections)
            ShowDirections();

        //Sprite Flip
        if (rb.velocity.x > 0f)
            spriteT.localScale = originalScale;
        if (rb.velocity.x < 0f)
            spriteT.localScale = new Vector3(-originalScale.x, originalScale.y, originalScale.z);

        //Animator Parameters
        animator.SetFloat("Vertical Velocity", rb.velocity.y);
        animator.SetFloat("Vertical Speed", Mathf.Abs(rb.velocity.y));
        animator.SetFloat("Horizontal Speed", Mathf.Abs(rb.velocity.x));
        animator.SetBool("Moving", rb.velocity != Vector2.zero);
        animator.SetFloat("Speed", rb.velocity.magnitude);
        animator.SetBool("Diving", diving);
    }

    private void Movimentation()
    {
        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");
        Vector2 inputVector = new Vector2(x, y);
        rb.velocity = inputVector.normalized * speed;
    }

    IEnumerator PickRoutine(float pickDuration)
    {
        animator.SetBool("Picking", true);
        canControl = false;
        rb.velocity = Vector2.zero;
        foreach (Wearing w in wearing)
            w.SelfDestroy();
        wearing.Clear();

        yield return new WaitForSeconds(pickDuration);
        animator.SetBool("Picking", false);
        canControl = true;
    }


    void DiveRun()
    {
        Debug.Log("Dive Run preparation");
        if (canDiveFromUpwards)
        {
            StartCoroutine(DiveRountine(Vector2.down * speed));
        }
        else if (canDiveFromRight)
        {
            StartCoroutine(DiveRountine(Vector2.left * speed));
        }
        else if (canDiveFromDownwards)
        {
            StartCoroutine(DiveRountine(Vector2.up * speed));
        }
        else if (canDiveFromLeft)
        {
            StartCoroutine(DiveRountine(Vector2.right * speed));
        }
        else
        {
            Debug.Log("Cant dive from here");
        }

    }

    void ShowDirections()
    {
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            foreach (GameObject direction in directions)
                direction.SetActive(false);
            directions[0].SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            foreach (GameObject direction in directions)
                direction.SetActive(false);
            directions[1].SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            foreach (GameObject direction in directions)
                direction.SetActive(false);
            directions[2].SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            foreach (GameObject direction in directions)
                direction.SetActive(false);
            directions[3].SetActive(true);
        }
    }

    public void SplashSound() { AudioManager.instance.Play("Splash"); }
    public void AfterJump() { rb.velocity = Vector2.zero; }

    IEnumerator DiveRountine(Vector2 runVelocity)
    {
        //Debug.Log("Entered Routine");
        diveCam.SetActive(true);
        canControl = false;
        canJump = false;
        boxCollider2D.isTrigger = true;
        canExit = false;
        yield return null;

        rb.velocity = runVelocity;
        //Debug.Log("Speed setted: " + runVelocity);
        yield return new WaitUntil(() => canJump);
        diving = true;
        yield return null;
        animator.SetTrigger("Jump");

        yield return new WaitForSeconds(2);
        AfterJump();
        transform.position = boxCollider2D.GetComponent<Transform>().position + Vector3.up * 0.5f;
        canDiveFromUpwards = false;
        canDiveFromRight = false;
        canDiveFromDownwards = false;
        canDiveFromLeft = false;
        canExit = true;
        animator.SetTrigger("Emerge");
        showDirections = true;
        //Debug.Log("Choose your direction");
        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.O) && 
        (directions[0].activeSelf || directions[1].activeSelf || directions[2].activeSelf || directions[3].activeSelf));

        if (directions[0].activeSelf)
            rb.velocity = Vector2.up * speed;

        else if (directions[1].activeSelf)
            rb.velocity = Vector2.right * speed;

        else if (directions[2].activeSelf)
            rb.velocity = Vector2.down * speed;

        else if (directions[3].activeSelf)
            rb.velocity = Vector2.left * speed;

        else
            Debug.LogWarning("Error on finding Exit direction");
        showDirections = false;
        animator.SetTrigger("Exit");
        foreach (GameObject direction in directions)
            direction.SetActive(false);
        AudioManager.instance.Play("Water Exit");
        diveCam.SetActive(false);
        yield return new WaitUntil(() => !diving);

        boxCollider2D.isTrigger = false;
        canControl = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<DiveSpot>() != null)
        {
            DiveSpot diveSpot = collision.GetComponent<DiveSpot>();
            boxCollider2D = diveSpot.lake;
            switch (diveSpot.side)
            {
                case DiveSpot.Border.Up:
                    canDiveFromUpwards = true;
                    break;
                case DiveSpot.Border.Right:
                    canDiveFromRight = true;
                    break;
                case DiveSpot.Border.Down:
                    canDiveFromDownwards = true;
                    break;
                case DiveSpot.Border.Left:
                    canDiveFromLeft = true;
                    break;
                default:
                    Debug.LogWarning("No side assigned to DiveSpot: " + diveSpot.gameObject.name);
                    break;
            }
        }
        if (collision.gameObject.CompareTag("Lake") && !canControl)
        {
            canJump = true;
        }
        if (collision.gameObject.GetComponent<Wearing>() != null)
            wearing.Add(collision.gameObject.GetComponent<Wearing>());
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<DiveSpot>() != null && !diving)
        {
            DiveSpot diveSpot = collision.GetComponent<DiveSpot>();
            switch (diveSpot.side)
            {
                case DiveSpot.Border.Up:
                    canDiveFromUpwards = false;
                    break;
                case DiveSpot.Border.Right:
                    canDiveFromRight = false;
                    break;
                case DiveSpot.Border.Down:
                    canDiveFromDownwards = false;
                    break;
                case DiveSpot.Border.Left:
                    canDiveFromLeft = false;
                    break;
                default:
                    Debug.LogWarning("No side assigned to DiveSpot: " + diveSpot.gameObject.name);
                    break;
            }

        }
        if (collision.gameObject.CompareTag("Lake") && canExit)
        {
            diving = false;
        }
        if (collision.gameObject.GetComponent<Wearing>() != null)
            wearing.Remove(collision.gameObject.GetComponent<Wearing>());
    }
}
