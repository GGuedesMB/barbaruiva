using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    [Header("Parameters")]
    [SerializeField] float followRange;
    [SerializeField] float followSpeed;

    [Header("References")]
    [SerializeField] Transform player;

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, followRange);
    }



    public IEnumerator CameraShake(float duration, float magnitude)
    {
        Vector3 originalPos = transform.localPosition;
        float elapsed = 0.0f;

        while (elapsed < duration)
        {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;

            transform.localPosition = new Vector3(x, y, originalPos.z);

            elapsed += Time.unscaledDeltaTime;

            yield return null;
        }

        transform.localPosition = originalPos;
    }

    private void Update()
    {
        Follow();
    }

    public void Follow()
    {
        Vector3 distance = new Vector3 (player.transform.position.x - transform.position.x, player.transform.position.y - transform.position.y, 0);

        if (distance.magnitude > followRange)
            transform.position += distance.normalized * followSpeed * Time.deltaTime;

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -6.5f, 6.5f), Mathf.Clamp(transform.position.y, -10, 9.6f), transform.position.z);
        
    }
}
