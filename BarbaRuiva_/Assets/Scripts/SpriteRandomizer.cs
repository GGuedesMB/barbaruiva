using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteRandomizer : MonoBehaviour
{
    [SerializeField] GameObject[] sprites;

    int spriteIndex;

    // Start is called before the first frame update
    void Start()
    {
        Randomize();
    }

    public void Randomize()
    {
        spriteIndex = Random.Range(0, sprites.Length);
        for(int i = 0; i < sprites.Length; i++)
        {
            if (i == spriteIndex)
                sprites[i].SetActive(true);
            else
                sprites[i].SetActive(false);
        }
    }
}
