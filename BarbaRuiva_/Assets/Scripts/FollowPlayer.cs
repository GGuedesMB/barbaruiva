﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [Header("Follow  raycast checkers")]
    [SerializeField] Transform upCheck;
    [SerializeField] Transform rightCheck;
    [SerializeField] Transform downCheck;
    [SerializeField] Transform leftCheck;
    [SerializeField] float checkRadius;
    [SerializeField] LayerMask checkLayer;

    Rigidbody2D rb;
    BarbaController barba;

    bool upRay;
    bool rightRay;
    bool downRay;
    bool leftRay;
    Vector2 distance;
    bool avoiding;

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(upCheck.position, checkRadius);
        Gizmos.DrawWireSphere(rightCheck.position, checkRadius);
        Gizmos.DrawWireSphere(downCheck.position, checkRadius);
        Gizmos.DrawWireSphere(leftCheck.position, checkRadius);
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        barba = FindObjectOfType<BarbaController>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        upRay = Physics2D.CircleCast(upCheck.position, checkRadius, Vector2.up, 0, checkLayer);
        downRay = Physics2D.CircleCast(downCheck.position, checkRadius, Vector2.up, 0, checkLayer);
        rightRay = Physics2D.CircleCast(rightCheck.position, checkRadius, Vector2.up, 0, checkLayer);
        leftRay = Physics2D.CircleCast(leftCheck.position, checkRadius, Vector2.up, 0, checkLayer);

        if (!avoiding && !barba.diving)
            Follow(2f);
        else if (barba.diving)
            rb.velocity = Vector2.zero;
    }

    public void Follow(float speed)
    {
        distance = barba.transform.position - transform.position;
        rb.velocity = distance.normalized * speed;

        if (distance.y > 0 && upRay)
            StartCoroutine(BlockedUpwards(Vector2.right * speed * Mathf.Sign(distance.x)));

        if (distance.y < 0 && downRay)
            StartCoroutine(BlockedDownwards(Vector2.right * speed * Mathf.Sign(distance.x)));

        if (distance.x > 0 && rightRay)
            StartCoroutine(BlockedRightwards(Vector2.up * speed * Mathf.Sign(distance.y)));

        if (distance.x < 0 && leftRay)
            StartCoroutine(BlockedLeftwards(Vector2.up * speed * Mathf.Sign(distance.y)));

    }

    IEnumerator BlockedUpwards(Vector2 velocity)
    {
        avoiding = true;
        rb.velocity = velocity;
        yield return new WaitUntil(() => !upRay || distance.y <= 0 || barba.diving);
        yield return new WaitForSeconds(0.3f);
        avoiding = false;
    }
    IEnumerator BlockedDownwards(Vector2 velocity)
    {
        avoiding = true;
        rb.velocity = velocity;
        yield return new WaitUntil(() => !downRay || distance.y >= 0 || barba.diving);
        yield return new WaitForSeconds(0.3f);
        avoiding = false;
    }
    IEnumerator BlockedRightwards(Vector2 velocity)
    {
        avoiding = true;
        rb.velocity = velocity;
        yield return new WaitUntil(() => !rightRay || distance.x <= 0 || barba.diving);
        yield return new WaitForSeconds(0.3f);
        avoiding = false;
    }

    IEnumerator BlockedLeftwards(Vector2 velocity)
    {
        avoiding = true;
        rb.velocity = velocity;
        yield return new WaitUntil(() => !leftRay || distance.x >= 0 || barba.diving);
        yield return new WaitForSeconds(0.3f);
        avoiding = false;
    }
}
