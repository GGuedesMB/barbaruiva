﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Wearing>() != null)
        {
            //Debug.Log("On enter");
            FindObjectOfType<WearingSpreader>().ChangePosition(collision.GetComponent<Transform>());
        }

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Wearing>() != null)
        {
            //Debug.Log("On stay");
            FindObjectOfType<WearingSpreader>().ChangePosition(collision.GetComponent<Transform>());
        }

    }
}
