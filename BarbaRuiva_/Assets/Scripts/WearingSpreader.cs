﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WearingSpreader : MonoBehaviour
{
    [SerializeField] int quantity;
    [SerializeField] Vector4 bounds;
    [SerializeField] GameObject wearingPrefab;
    [SerializeField] Transform wearingsParent;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < quantity; i++)
        {
            float x = Random.Range(bounds.x, bounds.y);
            float y = Random.Range(bounds.z, bounds.w);

            GameObject wearing = Instantiate(wearingPrefab, new Vector2(x, y), Quaternion.identity);
            wearing.transform.parent = wearingsParent;
        }
        
    }


    public void ChangePosition(Transform wearing)
    {
        float x = Random.Range(bounds.x, bounds.y);
        float y = Random.Range(bounds.z, bounds.w);
        //Debug.Log(wearing.position);
        wearing.position = new Vector2(x, y);

    }
}
