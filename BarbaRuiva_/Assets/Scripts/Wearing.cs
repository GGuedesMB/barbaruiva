﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wearing : MonoBehaviour
{

    public void SelfDestroy()
    {
        AudioManager.instance.Play("Pick");
        Destroy(gameObject, 0.1f);
    }
}
